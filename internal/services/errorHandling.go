package services

import (
	"errors"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type ServerErrorResponse struct {
	Message string `json:"message"`
}

func ErrorResponse(c *gin.Context, err string, exception error) error {
	logrus.Error(err)
	if exception != nil {
		logrus.Error("Detailed Error: " + exception.Error())
	}
	c.JSON(400, ServerErrorResponse{
		Message: err,
	})
	return errors.New(err)
}

func InternalErrorResponse(c *gin.Context, err string, exception error) error {
	logrus.Error(err)
	if exception != nil {
		logrus.Error("Detailed Error: " + exception.Error())
	}

	c.JSON(500, ServerErrorResponse{
		Message: err,
	})
	return errors.New(err)
}
